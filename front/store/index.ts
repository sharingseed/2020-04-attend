import Vue from "vue"
import Vuex from 'vuex'
import { extractVuexModule, createProxy } from "vuex-class-component"
import app from "./app"

Vue.use(Vuex)

export const store = new Vuex.Store({
  modules: {
    ...extractVuexModule(app),
  }
})
export const vx = {
  app: createProxy(store, app),
}

declare module "vue/types/vue" {
  interface Vue {
    $vx: typeof vx
  }
}

/** 初期化 */
export const initializeStore = async (app) => {
  for (const [storeId, proxy] of Object.entries(vx)) {
    // proxy.persistKeysに対応する要素の永続化
    const itemIds = proxy["persistKeys"] || []
    itemIds.forEach(itemId => {
      const key = `store.${storeId}.${itemId}`
      const json = window.localStorage.getItem(key)
      if (json) {
        const { value } = JSON.parse(json)
        proxy[itemId] = value
        console.info("store.restore", { storeId, itemId, value })
      }
      proxy.$watch(itemId, value => {
        window.localStorage.setItem(key, JSON.stringify({ value }))
        console.info("store.save", { storeId, itemId, value })
      })
    })
    // proxy.initializeStoreの呼び出し
    if (proxy["initializeStore"]) {
      await proxy["initializeStore"](app)
    }
  }
}

export default () => store