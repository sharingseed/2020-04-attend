import { action, createModule } from "vuex-class-component"

export default class extends createModule({
  namespaced: "app", target: "nuxt"
}) {
  loggedInAccessToken = ""

  /** 永続化する要素の指定 */
  persistKeys = ["loggedInAccessToken"]
  /** 初期化処理 */
  @action async initializeStore(app) {
    // ログイン済みユーザの情報取得など
  }
}