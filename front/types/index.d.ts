 // Fetch用の型定義
 declare type GlobalFetch = WindowOrWorkerGlobalScope
  
 // Promiseの中身の型を取り出す
 type UnpackPromise<T> = T extends Promise<infer U> ? U : T
 
 // Promiseになっている戻り値の中身の型を得る
 type ReturnPromise<T> = T extends (...args)=>any ? UnpackPromise<ReturnType<T>> : never