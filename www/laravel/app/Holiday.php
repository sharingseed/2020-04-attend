<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Holiday extends Model
{
    //update_at / create_at なし。
    public $timestamps = false;
    //
    protected $fillable = [
        'date', 'title'
    ];
}
