<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AttendsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $oldTable = DB::table('Attend')->get();

        foreach($oldTable as $oldRecord){
            foreach($oldRecord as $name => $value){
                if($name == 'reg_date') $newRecord['created_at'] = $value;
                elseif($name == 'update_date') $newRecord['updated_at'] = $value;
                else $newRecord[$name] = $value;
            }
            $newTable[] = $newRecord;
        }

        print_r("旧テーブルレコード数：".count($oldTable));
        echo "\n";
        print_r("新テーブルレコード数：".count($newTable));
        echo "\n";

        DB::table('attends')->truncate();
        DB::table('attends')->insert($newTable);
    }
}
