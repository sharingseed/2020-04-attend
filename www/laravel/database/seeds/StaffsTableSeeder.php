<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;


class StaffsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $oldTable = DB::table('Staff')->get();

        foreach($oldTable as $oldRecord){
            foreach($oldRecord as $name => $value){
                if($name == 'reg_date') $newRecord['created_at'] = $value;
                elseif($name == 'update_date') $newRecord['updated_at'] = $value;
                elseif($name == 'del_flg') $newRecord['deleted_at'] = ($value == 1) ? date("Y-m-d H:i:s") : null;
                else $newRecord[$name] = $value;
            }
            $newTable[] = $newRecord;
        }

        print_r("旧テーブルレコード数：".count($oldTable));
        echo "\n";
        print_r("新テーブルレコード数：".count($newTable));
        echo "\n";

        DB::table('staffs')->truncate();
        DB::table('staffs')->insert($newTable);
    }
}
