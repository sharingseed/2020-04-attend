<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class HolidaysTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $oldTable = DB::table('Holiday')->get();

        foreach($oldTable as $oldRecord){
            foreach($oldRecord as $name => $value){
                $newRecord[$name] = $value;
            }
            $newTable[] = $newRecord;
        }

        print_r("旧テーブルレコード数：".count($oldTable));
        echo "\n";
        print_r("新テーブルレコード数：".count($newTable));
        echo "\n";

        DB::table('holidays')->truncate();
        DB::table('holidays')->insert($newTable);
    }
}
