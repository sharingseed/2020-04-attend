<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSpecialHolidaysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('special_holidays', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('staff_id')->nullable();
            $table->integer('type')->nullable();
            $table->date('date')->nullable();
            $table->longtext('reason')->nullable();
            $table->longText('private_memo')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('special_holidays');
    }
}
